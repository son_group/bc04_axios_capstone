function renderProduct(productList) {
  let contentHTML = `<div class="row">`;
  for (let product of productList) {
    let productContent = `
    <div class="col-12 col-md-6 col-lg-4 my-2" >
        <div class="card cardPhone">
            <img src="${product.img}" class="card-img-top mt-4" alt="..." />
            <div class="card-body">
                <div class=" ">
                    
                        <h4 class="cardPhone__text mr-2">Name: ${product.name}</h4>
                        <h4 class="cardPhone__text">$ ${product.price}</h4>
                    
                </div>
                <div class="">
                        <p class="cardPhone__text">${product.desc}</p>
                        <p class="cardPhone__text">${product.type}</p>
                        <div>
                            <button onclick="addProductToCart('${product.id}')" class="btnPhone-shadow">
                                Add to cart
                            </button>
                        </div>
                </div>
                
            </div>
        </div>
    </div>`;
    contentHTML += productContent;
  }
  // console.log("contentHTML ", contentHTML);
  document.getElementById("product_list").innerHTML = contentHTML;
}

function renderListType(productList) {
  // console.log("List type:", productList);
  let typeList = [];
  for (let product of productList) {
    let index = typeList.indexOf(product.type);
    // console.log("index", index);
    if (index == -1) {
      // console.log("product.type ", product.type);
      typeList.push(product.type);
    }
  }
  let selector = document.getElementById("phoneType");
  let optionDefault = document.createElement("option");
  optionDefault.innerHTML = "Chọn hãng";
  optionDefault.setAttribute("selected", true);
  selector.appendChild(optionDefault);

  typeList.forEach((element) => {
    let opt = document.createElement("option");
    opt.value = element;
    opt.innerHTML = element;
    selector.appendChild(opt);
  });
}

function getProductFilter(productList, type) {
  let productFilterList = productList.filter((product) => product.type == type);
  return productFilterList;
}

// const CARTLIST_LOCALSTORAGE = "CARTLIST_LOCALSTORAGE";

let productList = [];
let cartList = [];
if (localStorage.getItem("cart")) {
  cartList = JSON.parse(localStorage.getItem("cart"));
}

const BASE_URL = "https://62db6cabe56f6d82a7728576.mockapi.io/";

function getProductList() {
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      //   tatLoading();
      // console.log(res);
      productList = res.data;
      // console.log("Product list ", productList);
      document.getElementById("phoneType").style.display = "block";
      renderProduct(productList);
      renderListType(productList);

      // console.log("productList", productList);
    })
    .catch(function (err) {
      //   tatLoading();
      console.log(err);
    });
}
getProductList();

function productFilter() {
  let type = document.getElementById("phoneType").value;
  let productFilter = getProductFilter(productList, type);
  type.localeCompare("Chọn hãng")
    ? renderProduct(productFilter)
    : renderProduct(productList);
}

function saveCartToLocalStorage() {
  window.localStorage.setItem("cart", JSON.stringify(cartList));
}

function addProductToCart(id) {
  let index = productList.findIndex((product) => product.id == id);
  let cartItem = { ...productList[index], quantity: 1 };
  let indexCart = cartList.findIndex((item) => item.id === id);
  indexCart === -1 ? cartList.push(cartItem) : cartList[indexCart].quantity++;
}

function increaseQuantity(booleanValue, id) {
  let index = cartList.findIndex((item) => {
    return item.id == id;
  });
  // console.log("index", index);
  if (index != -1 && booleanValue == true) {
    cartList[index].quantity += 1;
    // console.log("quantity ", cartList[index].quantity);
  }

  if (index != -1 && booleanValue == false) {
    cartList[index].quantity > 1
      ? (cartList[index].quantity -= 1)
      : cartList[index].quantity;
    // console.log("quantity ", cartList[index].quantity);
  }
  renderCart(cartList);
}

function deleteProduct(id) {
  let index = cartList.findIndex((item) => {
    return item.id == id;
  });
  if (index != -1) {
    cartList.splice(index, 1);
    renderCart(cartList);
  }
}

function payout() {
  cartList = [];
  renderCart(cartList);
}

function viewCart() {
  // console.log("cart list", cartList);
  renderCart(cartList);
  document.getElementById("phoneType").style.display = "none";
  saveCartToLocalStorage();
}

function ProductsService() {
  const BASE_URL = "https://62db6cabe56f6d82a7728576.mockapi.io/products";
  this.layDSSP = function () {
    //GET: lấy dữ liệu từ data
    var promise = axios({
      method: "get",
      url: BASE_URL,
    });

    return promise;
  };
  this.themSP = function (sp) {
    //POST: Thêm mới dữ liệu
    //data: dữ liệu cần thêm vào Cơ sở dữ liệu
    var promise = axios({
      method: "post",
      url: BASE_URL,
      data: sp,
    });

    return promise;
  };
  this.xoaSanPham = function (id) {
    //DELETE: xóa data dựa vào id
    var promise = axios({
      method: "delete",
      url: `${BASE_URL}/${id}`,
    });

    return promise;
  };
  this.xemSanPham = function (id) {
    //GET: lấy data cua 1 sản phẩm dựa vào id
    var promise = axios({
      method: "get",
      url: `${BASE_URL}/${id}`,
    });

    return promise;
  };
  this.capNhatSanPham = function (id, sp) {
    //PUT: cập nhật data của 1 sản phẩm dựa vào id
    var promise = axios({
      method: "put",
      url: `${BASE_URL}/${id}`,
      data: sp,
    });

    return promise;
  };

  this.timKiemSP = function (mangSP, chuoiTK) {
    var mangTK = [];
    mangTK = mangSP.filter(function (sp) {
      return sp.tenSP.toLowerCase().indexOf(chuoiTK.toLowerCase()) >= 0;
    });
    return mangTK;
  };
}
